const assert = require("assert")
const {parseLightseekers, generateLightseekers} = require("../lib/index")
const _ = require("lodash")

const testDeck = [
  {id: 51377903, count: 1, variant: 1},
  {id: 50431448, count: 2, variant: 1},
  {id: 77349409, count: 1, variant: 1}
]

// Generate and then parse
const code = generateLightseekers(testDeck)
assert(code === "_wLv9g8DAdiFAYMBIUKcRAE=")

const expectedOutput = _.cloneDeep(testDeck)
expectedOutput[0].hero = true

assert(_.isEqual({
  version: 2,
  cards: expectedOutput
}, parseLightseekers(code)))

// Check generated codes use sorted deck
const shuffledTestDeck = [
  testDeck[0],
  testDeck[2],
  testDeck[1]
]
assert(_.isEqual(code, generateLightseekers(shuffledTestDeck)))

// Parse a predefined deck
assert(_.isEqual({
    version: 1,
    cards: [
      {count: 1, hero: true, id: 51732013},
      {count: 2, id: 50431448},
      {count: 1, id: 50558596},
      {count: 2, id: 50643065},
      {count: 1, id: 50720277},
      {count: 1, id: 50721048},
      {count: 1, id: 50864758},
      {count: 2, id: 51015189},
      {count: 1, id: 51251861},
      {count: 2, id: 51298592},
      {count: 2, id: 51377903},
      {count: 2, id: 51527119},
      {count: 1, id: 51551484},
      {count: 1, id: 51596665},
      {count: 1, id: 51966011},
      {count: 1, id: 52107454},
      {count: 1, id: 52136252},
      {count: 1, id: 52246433},
      {count: 2, id: 52596810},
      {count: 1, id: 52611439},
      {count: 2, id: 52829815},
      {count: 2, id: 52850915},
      {count: 1, id: 53145738},
      {count: 1, id: 53369675},
      {count: 1, id: 54449003},
      {count: 1, id: 77349409},
      {count: 1, id: 79106201}
    ]
  },
  parseLightseekers("_wEtXhUD2IUBg4R2A0N5wASDFe4FQxjxBUN2IghDFW4Kg5UKDkMgwQ6D7_YPg889EoP8nBJDeU0TQzvwGEO-GBtDPIkbQ6E3HUNKkCKDb8kiQ3ceJoPjcCaDivAqQ0tbLkNr0z5DIUKcRJkQt0Q")
))

// V2

const v2code = "_wK82IgAAXmmlUEBaRAIwwHEfQhDASd4GkMBYO0bQwGvER-DAdtLI8MBSLIjQwGgwCWDAc3uMEMBRqgywwFBGTlDATUAlcMBOV1NhAESpJlEAVT_EYUBoForjAG5FK_MAZeIqo0B"
assert(generateLightseekers(parseLightseekers(v2code).cards) === v2code)

assert(_.isEqual({
    version: 2,
    cards:
      [
        {count: 1, hero: true, id: 8968380, variant: 1},
        {count: 1, id: 26584697, variant: 1},
        {count: 3, id: 50860137, variant: 1},
        {count: 1, id: 50888132, variant: 1},
        {count: 1, id: 52066343, variant: 1},
        {count: 1, id: 52161888, variant: 1},
        {count: 2, id: 52367791, variant: 1},
        {count: 3, id: 52644827, variant: 1},
        {count: 1, id: 52671048, variant: 1},
        {count: 2, id: 52805792, variant: 1},
        {count: 1, id: 53538509, variant: 1},
        {count: 3, id: 53651526, variant: 1},
        {count: 1, id: 54073665, variant: 1},
        {count: 3, id: 60096565, variant: 1},
        {count: 2, id: 72179001, variant: 1},
        {count: 1, id: 77177874, variant: 1},
        {count: 2, id: 85065556, variant: 1},
        {count: 2, id: 204167840, variant: 1},
        {count: 3, id: 212800697, variant: 1},
        {count: 2, id: 229279895, variant: 1}
      ]
  },
  parseLightseekers(v2code)
))

// Variants

{
  const testDeck = [
    {id: 51377903, count: 1, variant: 1},
    {id: 50431448, count: 2, variant: 2},
    {id: 77349409, count: 1, variant: 50}
  ]

  // Generate and then parse
  const code = generateLightseekers(testDeck)

  const expectedOutput = _.cloneDeep(testDeck)
  expectedOutput[0].hero = true

  const parsed = parseLightseekers(code)
  assert(_.isEqual({
    version: 2,
    cards: expectedOutput
  }, parsed))
}

{
  // Variants are optional though, and defaults to 1
  const testDeck = [
    {id: 51377903, count: 1},
    {id: 50431448, count: 2},
    {id: 77349409, count: 1}
  ]

  // Generate and then parse
  const code = generateLightseekers(testDeck)

  const expectedOutput = _.cloneDeep(testDeck).map(c => {
    c.variant = 1
    return c
  })
  expectedOutput[0].hero = true

  const parsed = parseLightseekers(code)
  assert(_.isEqual({
    version: 2,
    cards: expectedOutput
  }, parsed))
}

console.log("Lightseekers tests passed!")